# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This Sandbox is a play/testing ground of different technologies in the java realm
* 0.1.x
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Maven 3.0.5  clean install package
* log4j (included)
* Maven, Tomcat (or servlet container of choice)
* Derby is configured, but only used in certain use cases.
* JUnit
* WAR File deployed to App/Servlet Container

### Contribution guidelines ###

* Writing tests : if you want)
* Code review  :: all Pull requests will be reviewed, but not ignored or denied (in reasonable situations) ... this is my playground afterall
* 

### Who do I talk to? ###

* R i c k    F i s h e r   f s h t a n k @  g m a  i l. c o m
* Other contact:  n/a  -- Army of one