package com.fishizle.springsample.acts.mindReader;

import com.fishizle.springsample.ApplicationContextUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/11/13
 * Time: 11:05 PM
 * To change this template use File | Settings | File Templates.
 */

public class MagicianTest {

    private String thoughts = "... TDD Thoughts in MagicianTest ...";
    private Magician magician = (Magician)
            ApplicationContextUtils.getApplicationContext().getAutowireCapableBeanFactory().getBean("magician");

    //@Test
    public void interceptThoughtsTest() {
        System.out.println("Intercepting Volunteer's thoughts: "+ thoughts);
        try {
            magician.interceptThoughts(thoughts);
            assert (magician.getThoughts().equals(thoughts));
        } catch (Exception e) {
            System.out.println("MagicianTest   Exception:"+e);
            assert (false);
        }
    }

    //@Test
    public void getThoughtsTest () {

        magician.interceptThoughts(thoughts);
        System.out.println("thinker.getThoughts()::'"+magician.getThoughts()+"' == '"+thoughts+"'");
        assert(magician.getThoughts().equals(thoughts));
    }
}
