package com.fishizle.springsample.acts.volunteers;

import com.fishizle.springsample.ApplicationContextUtils;
import com.fishizle.springsample.acts.Thinker;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/11/13
 * Time: 11:11 PM
 * To change this template use File | Settings | File Templates.
 */

public class VolunteerThinkerTest {

    private String thoughts = "VolunteerThinkerTest TDD Thoughts in VolunteerThingkerTest ...";
    private Thinker thinker  = (Thinker) ApplicationContextUtils.getApplicationContext().getBean("thinker");


    //@Test
    public void thinkOfSomethingTest() {
        try {
            thinker.thinkOfSomething(thoughts);
        } catch (Exception e) {
            System.out.println("thinkOfSomethingTest Exception:"+e);
            assert (false);
        }
    }

    //@Test
    public void getThoughtsTest() {
        thinker.thinkOfSomething(thoughts);
        System.out.println("thinker.getThoughts()::'"+thinker.getThoughts()+"' == '"+thoughts+"'");
        assert(thinker.getThoughts().equals(thoughts));
    }
}
