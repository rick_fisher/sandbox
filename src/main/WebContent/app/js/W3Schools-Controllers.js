/**
 * Created by rick on 6/14/15.
 */
var app = angular.module('myApp', []);

app.controller('MyController', function ($scope) {
    $scope.firstName1 = "John";
    $scope.lastName1 = "Doe";
});


app.controller('PersonController', function ($scope) {
    $scope.firstName2 = "Billy Ray";
    $scope.lastName2 = "Cyrus";
    $scope.fullName = function () {
        return $scope.firstName2 + " " + $scope.lastName2;
    }

    $scope.myVar = true;
    $scope.toggle = function() {
        $scope.myVar = !$scope.myVar;
    }
});


app.controller('NamesController', function($scope) {
// angular.module('anotherMyApp', []).controller('NamesController', function($scope) {
        $scope.names = [
            {name:'Jani',country:'Norway'},
            {name:'Hege',country:'Sweden'},
            {name:'Kai',country:'Denmark'}
        ];
    });


app.controller('CostController', function($scope) {
    $scope.price = 4;
    $scope.quantity = 225.00;

});



app.controller('CustomersController', function ($scope, $http) {
    /*$http.get("/data/customers.json")*/
    $http.get("http://www.w3schools.com/angular/customers.php")
        .success(function (response) {
            $scope.names = response.records;
        });
});


app.controller('CustomersSQLController', function($scope, $http) {
    $http.get("http://www.w3schools.com/angular/customers_mysql.php")
            .success(function (response) {
                $scope.names = response.records;
        });
});


app.controller('FormController', function($scope) {
    $scope.master = {firstName: "John", lastName: "Doe"};
    $scope.reset = function() {
        $scope.user = angular.copy($scope.master);
    };
    $scope.reset();
});
