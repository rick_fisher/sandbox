/**
 * Created by rick on 6/14/15.
 */
angular.module('indexApp', []).controller('IndexController', function($scope) {
    $scope.names = [
        {page:'index.html',             desc:'Index'},
        {page:'01.html',             desc:'run 01'},
        {page:'02-directives.html',  desc:'run 02 directives'},
        {page:'03-controllers.html', desc:'run 03 controllers'},
        {page:'run04-controllers.html', desc:'run 04 controllers'}
    ];
});