<%@ page import="com.fishizle.utility.FirewallFileUtility, com.scjp.chap01.inheritance.ClassTwo" %>
<%@ page import="java.net.InetAddress" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%--<jsp:useBean id="ClassTwo" beanName="c2" scope="request" type="com.scjp.chap01.inheritance.ClassTwo"/>--%>
    <title>JSP Page</title>
</head>
    <body>
    <div>
        <h3>Hello World!</h3>

        <%
            ClassTwo c2 = new ClassTwo();

            for (int x=0;x<2;x++) {    %>

                <p>
                Item # <%= x %> C1: <%=c2.showClassOneString() %>  ||  C3: <%=c2.showClassThreeString() %>
                </p>
        <%    }      %>
    </div>
    <div>

        <p1>URLS from File</p1>

        <%
            FirewallFileUtility f = new FirewallFileUtility();
            Map<String, String> map = f.getIPs();

            for (String addressKey : map.keySet()) { %>
                <p>
                <%=addressKey%> : <%=map.get(addressKey)%>
                </p>

            <%    }      %>
    </div>
    <div>
        <h3><a href="prettyprint">Apple Print</a></h3>
    </div>
    </body>
</html>