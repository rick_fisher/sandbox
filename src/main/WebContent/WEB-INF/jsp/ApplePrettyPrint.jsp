<%@ page import="java.util.ArrayList" %>
<%@ page import="com.fishizle.java8.model.Apple" %><%--
  Created by IntelliJ IDEA.
  User: rick
  Date: 4/14/16
  Time: 11:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ApplePrettyPrint</title>
    <style href="style/sandbox.css" />
</head>
<body>
    <jsp:useBean id="applePrint" class="com.fishizle.java8.chap2.print.ApplePrint" scope="session" />
    <jsp:useBean id="uiHelper" class="com.fishizle.java8.chap2.print.UIHelper" scope="session" />
    <jsp:useBean id="appleAdd" class="com.fishizle.java8.model.Apple" scope="request" />

    <%
        try {
            appleAdd.setColor((String) session.getAttribute("appleColor"));
            appleAdd.setWeight((int) session.getAttribute("appleWeight"));
        } catch (Exception e) {
            %> <%=e.getMessage() %> <%
        }
    %>

    <div>
        Apples List:
        <div>
            <table>
                <tr>
                    <th>Color</th><th>Weight</th>
                </tr>
                <tr>
                    <% for (Apple apple : uiHelper.getApplesList() ) {
                %> Color
            }
                %>
                    <td></td>
                    <td></td>
                </tr>
            </table>

        </div>
    </div>

    <div>
        <b>Add Apples</b>:<br>
        <form action="/prettyprint">
            <label>Color</label><input type="text" maxlength="20" size="25" name="appleColor" /><br>
            <label>Weight</label><input type="text" maxlength="10" size="10" name="appleWeight" /> <br>
            <input type="submit" value="Add Apple" />
        </form>
    </div>
</body>
</html>
