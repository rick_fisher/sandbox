package com.scjp.chap01.inheritance.level2;

import java.io.FileNotFoundException;

import org.apache.log4j.Logger;

public class ClassThree {

	Logger logger = Logger.getLogger(ClassThree.class);
	
	private String classThreeValue1="", classThreeValue2="";
	private Integer overLoadMe=null, overrideMe=null;
	int x=0;
	
	
	public void overLoadVoid() throws Exception {
		logger.debug("Class Three overLoadVoid");
	}
	
	public Integer getOverLoadMe() throws Exception, FileNotFoundException {
		return overLoadMe;
	}

	public void setOverLoadMe(Integer overLoadMe) {
		this.overLoadMe = overLoadMe;
	}

	public Integer getOverrideMe() {
		return overrideMe;
	}

	public void setOverrideMe(Integer overrideMe) {
		this.overrideMe = overrideMe;
	}

	public String getClassThreeValue1() {
		return classThreeValue1;
	}

	public void setClassThreeValue1(String value1) {
		this.classThreeValue1 = value1;
	}

	protected String getClassThreeValue2() {
		return classThreeValue2;
	}

	protected void setClassThreeValue2(String value2) {
		this.classThreeValue2 = value2;
	}
	
}
