package com.scjp.chap01.inheritance;

public abstract class AbstractOne {
		
	public abstract String doAbstractOne (String var1, Integer var2);
	
	public abstract Integer doAbstractTwo (String var1, Integer var2);
	
	public abstract Integer doAbstractThree (String var1, Integer var2);
	
	
}
