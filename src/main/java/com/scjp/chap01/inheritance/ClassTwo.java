package com.scjp.chap01.inheritance;
import org.apache.log4j.Logger;

import com.scjp.chap01.inheritance.level2.*;

public class ClassTwo {

	private static Logger logger = Logger.getLogger(ClassTwo.class);
	ClassThree classThree = new ClassThree();
	ClassOne classOne = new ClassOne();
	
	public ClassTwo() {
	}
	
	public void showClassOne() {
		classOne.setValue1("value1 from ClassTwo");
		classOne.setValue2("value2 from ClassTwo");
		logger.debug("ClassOne value1:" + classOne.getValue1());
		logger.debug("ClassOne value2:" + classOne.getValue2());
	}

	public String showClassOneString() {
		String retVal = "";
		classOne.setValue1("value1 from ClassTwo");
		classOne.setValue2("value2 from ClassTwo");
		retVal = "ClassOne value1:" + classOne.getValue1();
		retVal += "ClassOne value2:" + classOne.getValue2();
		return retVal;
	}
	
	// Cannot access protected methods outside package even when subclassed
	public void showClassThree() {
		classThree.setClassThreeValue1("Class Three value1 from ClassTwo");
		// classOne.setClassThreeValue2("Class Three value2 from ClassTwo"); 
		logger.debug("ClassOne value1:" + classThree.getClassThreeValue1());
		//System.out.println("ClassOne value2:" + classThree.getClassThreeValue2());
		// Default view variables or classes not available in different packages
		// logger.debug("ClassThree x="+classThree.x);
		
	}

	// Cannot access protected methods outside package even when subclassed
	public String showClassThreeString() {
		classThree.setClassThreeValue1("Class Three value1 from ClassTwo");
				String retVal = "ClassOne value1:" + classThree.getClassThreeValue1();
		return retVal;
	}
	
	
}
