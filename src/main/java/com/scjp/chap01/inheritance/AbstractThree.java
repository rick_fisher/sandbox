package com.scjp.chap01.inheritance;

public abstract class AbstractThree extends AbstractTwo {

	
	public abstract Integer goAbstractOne (String var1, Integer var2);
	
	public abstract Integer goAbstractTwo (String var1, Integer var2);

}
