package com.scjp.chap01.inheritance;

/**
 * 
 * @author Richard Fisher
 * Description:  Notice AbstractThree extends AbstractTwo.
 * This class has to extend all Abstract methods from both classes.
 */

public class ClassAbstractThreeImpl extends AbstractThree {

	
	@Override
	public Integer goAbstractOne(String var1, Integer var2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer goAbstractTwo(String var1, Integer var2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer processAbstractOne(String var1, Integer var2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer processAbstractThree(String var1, Integer var2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer processAbstractTwo(String var1, Integer var2) {
		// TODO Auto-generated method stub
		return null;
	}

}
