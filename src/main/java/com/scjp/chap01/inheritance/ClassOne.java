package com.scjp.chap01.inheritance;

import org.apache.log4j.Logger;

import com.scjp.chap01.inheritance.level2.ClassThree;

public class ClassOne extends ClassThree {
	
	public static final Logger logger = Logger.getLogger(ClassOne.class);
	
	static public String staticString="Class One Static String";
	private String value1="", value2="";
	
	
	
	//Var-Args
	public void tryVarArgs (String s, int...x) {
		for (int i=0;i<x.length;i++) {
			logger.info("Var Arg test x["+i+"]:"+x[i]);
		}
	}
	
	
	
	/*
	 * 
	 * Getters and Setters
	 * 
	 */
	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	protected String getValue2() {
		return value2;
	}

	protected void setValue2(String value2) {
		this.value2 = value2;
	}

	public ClassOne() {
	}
	
	@Override
	public Integer getOverLoadMe() throws Exception {
		return super.getOverLoadMe();
	}
	
	@Override
	public void overLoadVoid() throws Exception {
		logger.debug("Class One overLoadVoid");
	}
	
	public void overLoadVoid(String s) throws Exception {
		logger.debug("Class One overLoadVoid with string:"+s);
	}

}
