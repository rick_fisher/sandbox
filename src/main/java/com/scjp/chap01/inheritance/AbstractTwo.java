package com.scjp.chap01.inheritance;

public abstract class AbstractTwo {

	public abstract Integer processAbstractOne (String var1, Integer var2);
	
	public abstract Integer processAbstractTwo (String var1, Integer var2);
	
	public abstract Integer processAbstractThree (String var1, Integer var2);
	
}
