package com.scjp.chap01.variables;

public interface CoffeeSizeInt {

	final int BIG=0, HUGE=1, OVERWHELMING=2;
}
