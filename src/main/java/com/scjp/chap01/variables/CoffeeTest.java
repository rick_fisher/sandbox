package com.scjp.chap01.variables;

public class CoffeeTest {

	public static enum CoffeeSize {BIG, HUGE, OVERWHELMING};
	
	public void orderCoffee () {
		Coffee drink = new Coffee();
		drink.size= CoffeeSize.OVERWHELMING;
		System.out.println("Coffee Size: " + drink.size);
	}
	
	
	class Coffee {
		CoffeeSize size;
	}
	
}
