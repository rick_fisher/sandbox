package com.spring.aop.chap01.knight;

public interface Knight {
	public Object embarkOnQuest() throws QuestException;	
}
