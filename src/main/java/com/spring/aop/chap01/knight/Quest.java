package com.spring.aop.chap01.knight;

public interface Quest {

	public abstract Object embark() throws QuestException;
}
