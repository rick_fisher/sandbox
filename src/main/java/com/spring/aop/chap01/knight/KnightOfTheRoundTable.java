package com.spring.aop.chap01.knight;

import org.apache.log4j.Logger;

public class KnightOfTheRoundTable implements Knight {
	private static Logger logger = Logger.getLogger(KnightOfTheRoundTable.class);
	private Quest quest;
	
	public KnightOfTheRoundTable(String name) {
		// quest = new HolyGrailQuest();  // <-- Remove for SPRING
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object embarkOnQuest() throws QuestException {
		// TODO Auto-generated method stub
		logger.debug("embarkOnQuest - - START");
		return quest.embark();
	}
	
	
	public void setQuest(Quest quest) {
		logger.debug("setQuest - - START");
		this.quest = quest;
	}

	
	
}
