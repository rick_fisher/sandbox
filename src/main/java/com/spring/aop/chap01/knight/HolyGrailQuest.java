package com.spring.aop.chap01.knight;

public class HolyGrailQuest implements Quest {
	
	public HolyGrailQuest() {};
	
	public Object embark() throws QuestException {
		return new HolyGrail();
	}
}
