package com.spring.aop.chap01.knight;

public class QuestException extends Exception {

	public QuestException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public QuestException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public QuestException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public QuestException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
