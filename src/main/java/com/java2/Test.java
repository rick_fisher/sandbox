package com.java2;

import java.io.FileNotFoundException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

import com.java2.io.IOclass;
import com.java2.chap03.NumbersTest;
import com.java2.chap06.*;
import com.scjp.chap01.inheritance.ClassOne;
import com.scjp.chap01.inheritance.ClassTwo;
import com.scjp.chap01.inheritance.level2.ClassThree;
import com.java2.chap04.chap04;
import com.spring.aop.chap01.knight.KnightOfTheRoundTable;
import com.spring.aop.chap01.knight.QuestException;

import com.java2.utility.*;

public class Test {
	private static Logger logger = Logger.getLogger(Test.class);
	
	public static void main (String[] args) throws Exception {
		Test test = new Test();
		test.runJava2(6, args);
		
		 		
	}
	

	// Java Packages Research	
	public void runJava2 (int chapter, String args[]) throws Exception  {
		
		ClassOne classOne = null;
		ClassTwo classTwo = null;
		ClassThree classThree = null;
		
		switch (chapter) {
		case 99: 
			/* Java IO */
			IOclass ioc = new IOclass();
			ioc.readConsole();
			break;
		
		case 1:
			/* ClassOne SCJP */
			classOne = new ClassOne();
			// Var-args behaves the same w/wout an array
			int[] x = {0,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,98};  
			// classOne.tryVarArgs("Main", 0,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1);
			classOne.tryVarArgs("Main", x);
			break;
			
		case 2:
			/* ClassOne SCJP */
			logger.debug("ClassOne Overload Test");
			classOne = new ClassOne();
			try { classOne.overLoadVoid();
				logger.debug("Class One Static Object Reference:"+classOne.staticString);
				logger.debug("Class One Static Class Name Reference:"+ClassOne.staticString);
			} catch (Exception e) {}
			
			logger.debug("ClassOne IS-A ClassThree Overload Test");
			classThree = new ClassOne();
			try { classThree.overLoadVoid();
			} catch (Exception e) {}
			
			break;
		
		case 3:	
			logger.debug("Chap 03 Numbers Test");
			NumbersTest nt = new NumbersTest();
			//nt.invade((short) 7); // Have to Cast and int DOWN to a short (Cant go down, only up)
			//nt.question3(args);
			nt = nt.nt1;
			
			/*
			try {
				System.out.println("Double Parse Value:" + Double.parseDouble("H12456.778"));
			} catch (Exception e) {
				System.out.println("Exception: " + e);
				System.out.println("Exception: " + e.getMessage());
			}
			*/

			break;
		
		case 4:
			logger.debug("Chap 04 Class");
			
			chap04 chap4 = new chap04();
			chap4.process();
			break;
			
		case 6:
			logger.debug("Chap 06 Class");
			Chap6 chap6 = new Chap6();
			chap6.process(args);
			break;
			
		default:
			logger.debug("SCJP Chap 01 Switch DEFAULT");
			break;
		}
		
		
		logger.debug("Get JVM stats");
		SysUtils.getStats();
		
	}
	
	
	public void runSpringExamples() {
		Test test = new Test();
		// test.runJava2(0);
		try {
			test.runSpringInAction(1, "knight");
		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException e:"+e + "\r\n"+e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Exception e:"+e + "\r\n"+e.getMessage());
		}
	}
	
	
	/**
	 * @param chapter
	 * SPRING 
	 */
	public void runSpringInAction (int chapter, String example) throws FileNotFoundException {
		logger.debug("runSpringInAction - - START");
		String configFile="";
		
		/*
		 * Chapter 01 - Knight Example
		 */
		switch (chapter) {
			case 1: {
				logger.debug("Switch Case 1");
				configFile="C:/Projects/testProj/java2/src/com/java2/knight.xml";
				BeanFactory factory = new XmlBeanFactory(new FileSystemResource(configFile));
				KnightOfTheRoundTable knight = (KnightOfTheRoundTable) factory.getBean("knight");
				try {
					knight.embarkOnQuest();
				} catch (QuestException e) {
					System.out.println("QuestException:" + e.getMessage());
				}
				break;
			}
			case 2: {
				logger.debug("Switch Case 2");
				break;
			}
			case 3:  {
				logger.debug("Switch Case 3");
				break;
			}
			case 4:  {
				logger.debug("Switch Case 4");
				break;
			}
			default:  {
				logger.debug("Switch Case default");
				
			}
		}
		
		
		
	}
	
	
	
}
