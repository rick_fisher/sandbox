package com.java2.chap06;
import org.apache.log4j.*;

public class Chap6 {

	public static final Logger logger = Logger.getLogger(Chap6.class);
	
	
	public void process (String args[]) throws Exception {
		this.stringTest(args);
	}
	
	public void stringTest(String args[]) throws Exception {
		
		String s1="abcdef", s2 = s1; 
		s1=s1.concat(" New Addition");
		logger.debug("\r\nPart 1: S1:"+s1+"\r\nS2:"+s2);
		s2 = s1;
		s1 += " Ocean.  Why not?";
		logger.debug("\r\nPart 2: S1:"+s1+"\r\nS2:"+s2);
				
	}
}
