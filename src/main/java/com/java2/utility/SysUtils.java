package com.java2.utility;

public class SysUtils {

	public static void getStats() {
		 Runtime rt = Runtime.getRuntime();
		 System.out.println("Used: " + (rt.totalMemory() - rt.freeMemory()));
		 System.out.println("Free: " + rt.freeMemory());
		 System.out.println("Total: " + rt.totalMemory());

	}
}
