package com.java2.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IOclass {

	public void readConsole() {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Characters, q to quit");

		String str = "";

		try {
			do {
				str = br.readLine();
				System.out.println(str);
			} while (str != null && !str.equalsIgnoreCase("stop"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
