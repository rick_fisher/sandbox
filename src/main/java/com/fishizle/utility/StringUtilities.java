package com.fishizle.utility;


import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sznlf0 on 9/8/15.
 */
public class StringUtilities {

    String[] filenames = {"~/Documents/Firewall/PreProd_Clip_App_HTTP_addresses.txt"};

    List httpList = new ArrayList();
    static String HTTP="http://", HTTPS="https://";


    public void fileReader() {

        BufferedReader br = null;

        try {

            String sCurrentLine;

            br = new BufferedReader(new FileReader(filenames[0]));

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);

                if (sCurrentLine.contains(HTTPS)) {
                    httpList.add(StringUtils.substringAfter(HTTPS, null));
                }

                if (sCurrentLine.contains(HTTP)) {
                    httpList.add(StringUtils.substringAfter(HTTP, null));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }



}
