package com.fishizle.utility;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sznlf0 on 9/11/15.
 */
public class FirewallFileUtility {
    private static Logger logger = Logger.getLogger(FirewallFileUtility.class);


    public String DIRECTORY="/Users/sznlf0/Documents/Firewall";
    public String FILENAMES[] = {"wpsegment-origins.txt" };   // "PreProd_Clip_App_HTTP_addresses.txt", "Prod_Clip_App_HTTP_addresses.txt"};


    public Map<String, String> getIPs() {
        return this.readFiles();
    }


    /**
     *
     */
    private Map<String, String> readFiles() {
        logger.info("START readFiles");
        Map<String, String> ipMap = new HashMap<String, String>();

        File f=null;
        String sCurrentLine = "";
        BufferedReader br = null;

        for (int filenum=0;filenum < FILENAMES.length;filenum++) {

            String file = DIRECTORY + "/" + FILENAMES[filenum];
            try {
                br = new BufferedReader(new FileReader(file));

                while ((sCurrentLine = br.readLine()) != null) {
                    if (sCurrentLine !=null && sCurrentLine.trim() != "" && sCurrentLine.trim().length() > 0) {
                        int idx0 = sCurrentLine.indexOf("http");

                        if (idx0 >= 0) {
                            int idxStart = sCurrentLine.indexOf("//", idx0 + 1) + 2;

                            if (idxStart >0) {
                                int idxEnd = sCurrentLine.indexOf(":", idxStart);

                                if (idxEnd < 0) {
                                    idxEnd = sCurrentLine.indexOf("/", idxStart);
                                }

                                if (idxEnd > idxStart) {
                                    String hostName = sCurrentLine.substring(idxStart, idxEnd);

                                    if (hostName != null && hostName.trim() != "") {
                                        logger.info("added to array: " + hostName);

                                        String ip = this.getIpAddress(hostName);
                                        ipMap.put(ip, hostName);


                                    } else {
                                        logger.info("not added: " + hostName);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Error reading file " + file, e);
            } finally {
                try {
                    br.close();
                } catch (Exception e) {

                }
                f = null;
            }
        }

        return ipMap;
    }





    /**
     *
     * @param addressList
     * @return
     */
    public String getIpAddress(String hostName) {

        java.net.InetAddress inetAdd = null;
        String ip = null;

        int x=0;

        try {
            inetAdd = java.net.InetAddress.getByName(hostName);

        } catch (Exception e) {
            logger.error("Error in DNS Lookup ", e);
            ip = "unknown - " + x;
        }

        try {
            ip = new String(inetAdd.getAddress(), "UTF-8");
        } catch (Exception e) {
            ip = "Error converting inetAddress";
        }

        if (inetAdd == null) {
            ip = null;
        } else {
            ip = inetAdd.toString();
            ip = ip.substring(ip.indexOf("/")+1);
        }

        return ip;
    }


}
