package com.fishizle.web.servlet;

import com.scjp.chap01.inheritance.ClassTwo;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by sznlf0 on 9/3/15.
 */
public class TestServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        super.doGet(httpServletRequest, httpServletResponse);

        System.out.println("doGet");

        for (int x=0;x<2000;x++) {
            System.out.println(new ClassTwo().showClassOneString());
            System.out.println(new ClassTwo().showClassThreeString());
        }
    }

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        super.doPost(httpServletRequest, httpServletResponse);
    }

    @Override
    protected void doPut(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        super.doPut(httpServletRequest, httpServletResponse);
    }

    @Override
    protected void doDelete(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        super.doDelete(httpServletRequest, httpServletResponse);
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }
}
