package com.fishizle.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by sznlf0 on 9/8/15.
 */

@Controller
public class StringUtilController {


    // protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value = "/strings", method = RequestMethod.GET)
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // logger.info("Returning hello view");

        return new ModelAndView("string-utils");
    }


}
