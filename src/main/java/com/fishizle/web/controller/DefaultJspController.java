package com.fishizle.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by sznlf0 on 7/19/15.
 */
@Controller

public class DefaultJspController {

    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value = "/default-1", method = RequestMethod.GET)
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        logger.info("Returning hello view");

        return new ModelAndView("default");
    }


    @RequestMapping(value = "/default-2", method = RequestMethod.GET)
    public ModelAndView defaultRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        logger.info("Returning hello view v2");

        String message = "<br><div style='text-align:center;'>"
                + "<h3>********** default.htm - - </h3>This message is coming from CrunchifyHelloWorld.java **********</div><br><br>";
        return new ModelAndView("default", "message", message);
    }

}
