/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fishizle.springsample;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Richard Fisher
 */
public class Include2 {

    private Include3 incl3ByType;

    public void printMessage() {
        System.out.println(". . .  *** Include 2 message *** . . .");
        incl3ByType.printMessage();
    }

    public Include3 getIncl3ByType() {
        return incl3ByType;
    }

    @Autowired
    public void setIncl3ByType(Include3 incl3ByType) {
        this.incl3ByType = incl3ByType;
    }
}
