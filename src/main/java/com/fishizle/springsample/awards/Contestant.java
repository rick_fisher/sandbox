package com.fishizle.springsample.awards;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/17/13
 * Time: 12:00 AM
 * To change this template use File | Settings | File Templates.
 */
public interface Contestant {
    void receiveAward();
}
