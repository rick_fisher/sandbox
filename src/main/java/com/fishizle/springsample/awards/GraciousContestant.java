package com.fishizle.springsample.awards;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/17/13
 * Time: 12:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class GraciousContestant implements Contestant {
    @Override
    public void receiveAward() {
        System.out.println("\r\n\r\n *** AWARDS CEREMONY *** ");
        System.out.println("APPLAUSE .... ");
        System.out.println("First off, I'd like to thank God.");
        System.out.println("... More APPLAUSE .... ");
        System.out.println("I LOVE YOU MOM!!!!");
        System.out.println("... More APPLAUSE APPLAUSE APPLAUSE APPLAUSE APPLAUSE .... ");
        System.out.println("*** Exit AWARDS CEREMONY *** \r\n\r\n");
    }
}
