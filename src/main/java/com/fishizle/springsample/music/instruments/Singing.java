package com.fishizle.springsample.music.instruments;

import com.fishizle.springsample.music.Instrument;
import com.fishizle.springsample.music.qualifiers.VoiceInstrument;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/4/13
 * Time: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
@VoiceInstrument
public class Singing implements Instrument {
    @Override
    public void play() {
        //To change body of implemented methods use File | Settings | File Templates.
        System.out.println("Singing: Fa La La La La...");
    }
}
