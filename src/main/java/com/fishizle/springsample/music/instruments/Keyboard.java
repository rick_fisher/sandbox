package com.fishizle.springsample.music.instruments;

import com.fishizle.springsample.music.Instrument;
import com.fishizle.springsample.music.qualifiers.KeysInstrument;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/4/13
 * Time: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
@KeysInstrument
public class Keyboard  implements Instrument {
    @Override
    public void play() {
        //To change body of implemented methods use File | Settings | File Templates.
        System.out.println("Keyboard: Tink Tink Tink");
    }
}
