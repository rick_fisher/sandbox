package com.fishizle.springsample.music.instruments;

import com.fishizle.springsample.music.Instrument;
import com.fishizle.springsample.music.qualifiers.FourStringInstrument;
import com.fishizle.springsample.music.qualifiers.SixStringInstrument;
import com.fishizle.springsample.music.qualifiers.StringedInstrument;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/4/13
 * Time: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
@StringedInstrument
@FourStringInstrument
public class Bass implements Instrument {
    @Override
    public void play() {
        //To change body of implemented methods use File | Settings | File Templates.
        System.out.println("Bass: Brown Chicken Brown Cow");
    }
}
