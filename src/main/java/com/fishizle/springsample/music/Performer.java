package com.fishizle.springsample.music;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/4/13
 * Time: 9:30 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Performer {
    public void perform();
}
