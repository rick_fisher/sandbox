package com.fishizle.springsample.music.aspects;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/8/13
 * Time: 9:45 PM
 * To change this template use File | Settings | File Templates.
 */

public class Audience {
    public void takeSeats() {
        System.out.println("O-<-<  Audience taking Seats >->-o");
    }

    public void turnOffCellPhones() {
        System.out.println("O-<-<  Audience Turns off their phones >->-o");
    }

    public void applaud() {
        System.out.println("CLAP CLAP CLAP CLAP CLAP");
    }

    public void demandRefund() {
        System.out.println("Boo! Boo! Boo!  Want my moola back jack!");
    }

    public void watchPerformance(ProceedingJoinPoint joinPoint) {
        try {
            System.out.println(":: Around :: Audience Seated");
            System.out.println(":: Around :: Cell Phones off");
            long start=System.currentTimeMillis();

            joinPoint.proceed();

            long end=System.currentTimeMillis();

            System.out.println(":: around :: CLAP CLAP CLAP CLAP CLAP");
            System.out.println(":: around :: Performance took "+ (end-start) + " milliseconds.");
        } catch (Throwable t) {
            System.out.println(":: Around :: Boo! Want my moola back!");
        }

    }
}


