package com.fishizle.springsample.music;

import com.fishizle.springsample.music.qualifiers.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/4/13
 * Time: 9:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class Band {
    @Autowired
    @SkinsInstrument
    private Performer alex;

    @Autowired
    @StringedInstrument @FourStringInstrument
    private Performer michael;

    @Autowired
    @StringedInstrument @SixStringInstrument
    private Performer eddie;

    @Autowired
    @VoiceInstrument
    private Performer david;

    public void bandRocksOut() {
        alex.perform();
        eddie.perform();
        michael.perform();
        david.perform();
    }
}
