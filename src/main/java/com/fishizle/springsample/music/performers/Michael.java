package com.fishizle.springsample.music.performers;

import com.fishizle.springsample.music.Instrument;
import com.fishizle.springsample.music.Performer;
import com.fishizle.springsample.music.qualifiers.FourStringInstrument;
import com.fishizle.springsample.music.qualifiers.SixStringInstrument;
import com.fishizle.springsample.music.qualifiers.StringedInstrument;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/4/13
 * Time: 9:46 PM
 * To change this template use File | Settings | File Templates.
 */

@StringedInstrument @FourStringInstrument
public class Michael implements Performer {

    @Autowired
    @StringedInstrument
    @FourStringInstrument
    private Instrument instrument;

    @Override
    public void perform() {
        //To change body of implemented methods use File | Settings | File Templates.
        System.out.println("Michael Performs:");
        instrument.play();
    }
}
