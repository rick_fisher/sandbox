/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fishizle.springsample;

/**
 *
 * @author Richard Fisher
 */
public class Include1a {
   
    private Include2 include2;
    private WireByName wireByName;
    
    public void printMessage() {
        System.out.println(". . .  *** Include '1A' Sysout *** . . . ");
        include2.printMessage();
        wireByName.printMessage();
    }

    public Include2 getInclude2() {
        return include2;
    }

    public void setInclude2(Include2 include2) {
        this.include2 = include2;
    }

    public WireByName getWireByName() {
        return wireByName;
    }

    public void setWireByName(WireByName wireByName) {
        this.wireByName = wireByName;
    }
}
