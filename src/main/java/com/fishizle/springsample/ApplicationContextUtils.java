/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fishizle.springsample;

import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextUtils implements ApplicationContextAware {
 
  private static ApplicationContext ctx;
 
  @Override
  public void setApplicationContext(ApplicationContext appContext)
      throws BeansException {
    ctx = appContext;
 
  }
 
  public static ApplicationContext getApplicationContext() {
      if (ctx == null) 
          ctx= new ClassPathXmlApplicationContext("SpringSample_annotations.xml");
    return ctx;
  }
}
