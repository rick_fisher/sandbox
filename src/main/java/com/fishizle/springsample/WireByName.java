package com.fishizle.springsample;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/4/13
 * Time: 8:12 PM
 * To change this template use File | Settings | File Templates.
 */

public class WireByName {

    private WireByType wireByType;
    String inProp;

    public void printMessage() {
        System.out.println(". . . *** Wire By name is called w/Property ["+inProp+"] *** . . . ");
        wireByType.printMessage();
    }

    public String getInProp() {
        return inProp;
    }

    public void setInProp(String inProp) {
        this.inProp = inProp;
    }

    public WireByType getWireByType() {
        return wireByType;
    }

    public void setWireByType(WireByType wireByType) {
        this.wireByType = wireByType;
    }
}
