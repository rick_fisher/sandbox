/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fishizle.springsample;

import com.fishizle.springsample.music.Band;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Richard Fisher
 */
public class Startup {

    private IncludeClass include1;
    private ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();

    @Autowired
    private Band band;
    
    /**
     * 
     */
    public static void main(String[] args) {
        System.out.println("Application Started Successfully");
        Startup startup = new Startup();
        startup.goStartup();

    }

    public void goStartup() {
        include1 = (IncludeClass) ctx.getBean("include1");
        include1.printMessage();

        System.out.println("\r\n\r\n\r\nLet it Rock. Let it Roll:\r\n\r\n");

        band = (Band) ctx.getBean("band");
        band.bandRocksOut();
        
        this.javatest();
    }
    
    
    public void javatest() {
        for (int i=0; i<100000; i++) {
            System.out.println ("... running test "+ i);
        }
        System.out.println ("END");
        
    }
    
    
}
