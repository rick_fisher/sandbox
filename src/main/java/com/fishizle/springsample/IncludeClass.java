/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fishizle.springsample;

/**
 *
 * @author Richard Fisher
 */
public class IncludeClass {

    private Include1a includeOneA;
    private Include2 includeDos;
    private WireByName wireByName;
    
    public void printMessage() {
        System.out.println(". . .  *** Include Class Sysout *** . . . ");
        includeDos.printMessage();
        wireByName.printMessage();
        includeOneA.printMessage();
    }

    public Include2 getIncludeDos() {
        return includeDos;
    }

    public void setIncludeDos(Include2 includeDos) {
        this.includeDos = includeDos;
    }

    public Include1a getIncludeOneA() {
        return includeOneA;
    }

    public void setIncludeOneA(Include1a includeOneA) {
        this.includeOneA = includeOneA;
    }

    public WireByName getWireByName() {
        return wireByName;
    }

    public void setWireByName(WireByName wireByName) {
        this.wireByName = wireByName;
    }
}
