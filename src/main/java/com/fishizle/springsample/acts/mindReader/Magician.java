package com.fishizle.springsample.acts.mindReader;

import com.fishizle.springsample.acts.MindReader;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/11/13
 * Time: 11:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class Magician implements MindReader {

    private String thoughts;

    @Override
    public void interceptThoughts(String thoughts) {
        System.out.println("Intercepting Volunteer's thoughts: "+ thoughts);
        this.thoughts = thoughts;

    }

    @Override
    public String getThoughts() {
        return thoughts;
    }
}
