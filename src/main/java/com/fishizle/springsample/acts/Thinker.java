package com.fishizle.springsample.acts;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/11/13
 * Time: 11:11 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Thinker {
    void thinkOfSomething(String thoughts);
    String getThoughts();
}
