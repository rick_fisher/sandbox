package com.fishizle.springsample.acts;

/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/11/13
 * Time: 11:06 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MindReader {
    void interceptThoughts (String thoughts);
    String getThoughts();
}
