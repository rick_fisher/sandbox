package com.fishizle.springsample.acts.volunteers;

import com.fishizle.springsample.acts.Thinker;


/**
 * Created with IntelliJ IDEA.
 * User: Richard Fisher
 * Date: 8/11/13
 * Time: 11:11 PM
 * To change this template use File | Settings | File Templates.
 */

public class VolunteerThinker implements Thinker {

    private String thoughts;

    @Override
    public void thinkOfSomething(String thoughts) {
        this.thoughts=thoughts;
    }

    @Override
    public String getThoughts() {
        return thoughts;
    }
}
