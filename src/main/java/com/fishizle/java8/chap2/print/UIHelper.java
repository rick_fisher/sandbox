package com.fishizle.java8.chap2.print;

import com.fishizle.java8.model.Apple;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * Created by rick on 4/15/16.
 */
public class UIHelper {
    private List<Apple> applesList = new ArrayList<Apple>();

    public void addAppleToList(Apple apple) {
        applesList.add(apple);
    }

    public List<Apple> getApplesList() {
        return applesList;
    }


}
