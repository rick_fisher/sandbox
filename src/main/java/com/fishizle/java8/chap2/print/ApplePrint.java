/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fishizle.java8.chap2.print;

import com.fishizle.java8.model.Apple;
import java.util.List;

/**
 *
 * @author rick
 */
public class ApplePrint {
    
    public static void prettyPrintApple(List<Apple> inventory, ApplePredicate p) {
        String output; 
        for (Apple apple : inventory) {
            output = p.accept(apple);
            System.out.println(output);
        }
    }
    
}
