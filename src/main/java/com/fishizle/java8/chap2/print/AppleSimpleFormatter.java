/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fishizle.java8.chap2.print;

import com.fishizle.java8.model.Apple;

/**
 *
 * @author rick
 */
public class AppleSimpleFormatter implements ApplePredicate {

    @Override
    public String accept(Apple apple) {
        return "An apple of " + apple.getWeight() + "g ";
    }
    
}
