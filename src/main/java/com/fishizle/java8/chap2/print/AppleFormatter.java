package com.fishizle.java8.chap2.print;

import com.fishizle.java8.model.Apple;

/**
 * Created by rick on 4/13/16.
 */
public interface AppleFormatter {
    String accept (Apple a);
}
