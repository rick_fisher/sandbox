-- Table "offices"
CREATE TABLE `offices` (
   `officeCode`    VARCHAR(10)  NOT NULL,
   `city`          VARCHAR(50)  NOT NULL,
   `phone`         VARCHAR(50)  NOT NULL,
   `addressLine1`  VARCHAR(50)  NOT NULL,
   `addressLine2`  VARCHAR(50)  DEFAULT NULL,
   `state`         VARCHAR(50)  DEFAULT NULL,
   `country`       VARCHAR(50)  NOT NULL,
   `postalCode`    VARCHAR(15)  NOT NULL,
   `territory`     VARCHAR(10)  NOT NULL,
   PRIMARY KEY  (`officeCode`),
   INDEX (`phone`),
   INDEX (`city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Table "employees"
CREATE TABLE `employees` (
   `employeeNumber`  INT UNSIGNED  NOT NULL AUTO_INCREMENT,
   `lastName`        VARCHAR(50)   NOT NULL,
   `firstName`       VARCHAR(50)   NOT NULL,
   `extension`       VARCHAR(10)   NOT NULL,
   `email`           VARCHAR(100)  NOT NULL,
   `officeCode`      VARCHAR(10)   NOT NULL,
   `reportsTo`       INT UNSIGNED  DEFAULT NULL,
   `jobTitle`        VARCHAR(50)   NOT NULL,
   PRIMARY KEY  (`employeeNumber`),
   INDEX (`lastName`),
   INDEX (`firstName`),
   FOREIGN KEY (`reportsTo`) REFERENCES `employees` (`employeeNumber`)
      ON DELETE RESTRICT ON UPDATE CASCADE,
   FOREIGN KEY (`officeCode`) REFERENCES `offices` (`officeCode`)
      ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Table "customers"
CREATE TABLE `customers` (
   `customerNumber`   INT UNSIGNED  NOT NULL AUTO_INCREMENT,
   `customerName`     VARCHAR(50)   NOT NULL,
   `contactLastName`  VARCHAR(50)   NOT NULL,
   `contactFirstName` VARCHAR(50)   NOT NULL,
   `phone`            VARCHAR(50)   NOT NULL,
   `addressLine1`     VARCHAR(50)   NOT NULL,
   `addressLine2`     VARCHAR(50)   DEFAULT NULL,
   `city`             VARCHAR(50)   NOT NULL,
   `state`            VARCHAR(50)   DEFAULT NULL,
   `postalCode`       VARCHAR(15)   DEFAULT NULL,
   `country`          VARCHAR(50)   NOT NULL,
   `salesRepEmployeeNumber`  INT UNSIGNED  DEFAULT NULL,
   `creditLimit`      INT UNSIGNED  DEFAULT NULL,
   PRIMARY KEY (`customerNumber`),
   INDEX (`customerName`),
   INDEX (`contactLastName`),
   INDEX (`contactFirstName`),
   INDEX (`phone`),
   INDEX (`postalCode`),
   FOREIGN KEY (`salesRepEmployeeNumber`) REFERENCES `employees` (`employeeNumber`)
      ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Table "products"
CREATE TABLE `products` (
   `productCode`         VARCHAR(15)  NOT NULL,
   `productName`         VARCHAR(70)  NOT NULL,
   `productLine`         VARCHAR(50)  NOT NULL,
   `productScale`        VARCHAR(10)  NOT NULL,
   `productVendor`       VARCHAR(50)  NOT NULL,
   `productDescription`  TEXT         NOT NULL,  -- 64KB
   `quantityInStock`     SMALLINT     NOT NULL,  -- Allow negative
   `buyPrice`            DECIMAL(8,2) UNSIGNED  NOT NULL,
   `MSRP`                DECIMAL(8,2) UNSIGNED  NOT NULL,
   PRIMARY KEY (`productCode`),
   INDEX (`productName`),
   INDEX (`productVendor`),
   INDEX (`productLine`)    -- needed to be indexed to be used as foreign key
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Table "productlines"
CREATE TABLE `productlines` (
   `productLine`      VARCHAR(50)   NOT NULL,
   `textDescription`  VARCHAR(4000) DEFAULT NULL,
   `htmlDescription`  TEXT          DEFAULT NULL,  -- 64 KB
   `image`            BLOB          DEFAULT NULL,  -- 64 KB
   PRIMARY KEY (`productLine`),
   FOREIGN KEY (`productLine`) REFERENCES `products` (`productLine`)
      ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- You need to index the productLine column of the products table to use the column as a foreign key here.


-- Table "orders"
CREATE TABLE `orders` (
   `orderNumber`     INT UNSIGNED  NOT NULL AUTO_INCREMENT,
   `orderDate`       DATE          NOT NULL,
   `requiredDate`    DATE          NOT NULL,
   `shippedDate`     DATE          DEFAULT NULL,
   `status`          VARCHAR(15)   NOT NULL,  -- use ENUM
   `comments`        TEXT          DEFAULT NULL,
   `customerNumber`  INT UNSIGNED  NOT NULL,
   PRIMARY KEY  (`orderNumber`),
   INDEX (`orderDate`),
   INDEX (`customerNumber`),
   FOREIGN KEY (`customerNumber`) REFERENCES `customers` (`customerNumber`)
      ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Table "orderdetails"
CREATE TABLE `orderdetails` (
   `orderNumber`      INT UNSIGNED       NOT NULL,
   `productCode`      VARCHAR(15)        NOT NULL,
   `quantityOrdered`  SMALLINT UNSIGNED  NOT NULL,  -- [0, 65535]
   `priceEach`        DECIMAL(7,2)       NOT NULL,
   `orderLineNumber`  TINYINT UNSIGNED   NOT NULL,   -- [0,255]
   PRIMARY KEY  (`orderNumber`,`productCode`),
   FOREIGN KEY (`orderNumber`) REFERENCES `orders` (`orderNumber`)
      ON DELETE RESTRICT ON UPDATE CASCADE,
   FOREIGN KEY (`productCode`) REFERENCES `products` (`productCode`)
      ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Table "payments"
CREATE TABLE `payments` (
   `customerNumber`  INT UNSIGNED           NOT NULL,
   `checkNumber`     VARCHAR(50)            NOT NULL,
   `paymentDate`     DATE                   NOT NULL,
   `amount`          DECIMAL(8,2) UNSIGNED  NOT NULL,
   PRIMARY KEY  (`customerNumber`,`checkNumber`),
   FOREIGN KEY (`customerNumber`) REFERENCES `customers` (`customerNumber`)
      ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;